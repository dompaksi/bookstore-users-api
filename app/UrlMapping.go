package app

import (
	"gitlab.com/dompaksi/bookstore-users-api/controllers"
)

func mapUrls() {
	router.GET(
		"/ping",
		controllers.Ping,
	)
	router.GET("/users/", controllers.Ping)
}
