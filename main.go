package main

import (
	"gitlab.com/dompaksi/bookstore-users-api/app"
)

func main() {
	app.StartApplication()
}
